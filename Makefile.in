#apt-untangle Makefile.in

#RUN ./configure. DO NOT USE THIS FILE DIRECTLY

#Copyright 2015 Fedor Uvarov
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

#A plug in case it's used directly
all:
	#Please run ./configure. Do not use this file directly.
	exit 1

#The "#= COPY =" line makes configure start copying, "#= STOP =" pauses
#Another "#= COPY =" will then restart. It can be repeated until the end of file.
#WARNING: PARENTHESES MUST BE SCREENED AS {{ AND }}

#= COPY =
#=== CONFIGURATION ===
#SOURCE PATHS
#These should not be escaped
CONFIGURE = {Configure script path}
SRC = {Source directory}
SRC_ESC = '$(subst ','\'',$(SRC))'

#INSTALLATION PATHS
BINDIR = {Directory for executables}
MANDIR = {Directory for man pages}

#INSTALLATION OPTIONS
#Compile the man page?
ENABLE_DOC = {Enable documentation}
#Compress the man page with gzip?
COMPRESS_MAN = {Compress man pages}

#PROGRAM VERSION
ifeq "{Version}" "<not set, will be constructed automatically>"
HG = hg -R $(SRC_ESC)
VERSION := {Previous numbered release}+$(shell $(HG) log -r $$(basename -s '+' $$($(HG) identify --id)) --template 'hg~{{date|shortdate}}.{{node|short}}')
AUTO_VERSION = 1
else
#version must be a valid Debian version string
VERSION := {Version}
endif

#BUILT-IN SYSTEM SETTINGS
SHEBANG = {Python shebang}
PPAHOST = {PPA host}
CODENAME = {Distribution codename}
#TODO: write program_name into the program and into the man page
PROGRAM_NAME = {Program name}
PACKAGE_NAME = {Package name}

#ADDITIONAL PROGRAMS
PYTHON = python3
DOWNLOADER = curl --remote-name -L

#To be set if this is a released version with precompiled man
MAN_COMPILED = {man pages are precompiled}

#SOME HARDCODED SETTINGS
#Section 8 with the rest of APT utilities
MAN_SECTION = 8
DEFAULT_MAN_LANGUAGE = en
DEFMANDIR := $(MANDIR)/man$(MAN_SECTION)
TEST_DATA_URL = 'https://bitbucket.org/Acharvak/apt-untangle/downloads/apt-untangle_test_data.tar.xz'
DOWNLOADS_URL = 'https://bitbucket.org/Acharvak/apt-untangle/downloads'

#=== BUILD EVERYTHING (default target) ===
ifeq "$(ENABLE_DOC)" "True"
all: apt_untangle_configured.py doc
else
all: apt_untangle_configured.py
endif

.PHONY: all download-test-data check clean distclean maintainer-clean install install-doc uninstall uninstall-doc dist

apt_untangle_configured.py: config.status $(SRC)/apt_untangle.py
ifeq "$(VERSION)" ""
	@echo 'VERSION not set, nor assigned automatically! There must have been an error!'
	exit 1
endif 
	echo \#\!$(SHEBANG) > $@
	tail -n +2 $(SRC_ESC)/apt_untangle.py | \
	sed -e "s^PROGRAM_NAME = ''^PROGRAM_NAME = '$(PROGRAM_NAME)'^" \
	-e "s^PROGRAM_VERSION = ''^PROGRAM_VERSION = '$(VERSION)'^" \
	-e "s^PPA_HOST = ''^PPA_HOST = '$(PPAHOST)'^" \
	-e "s^OWN_PACKAGE_NAME = ''^OWN_PACKAGE_NAME = '$(PACKAGE_NAME)'^" \
	>> $@ || (rm $@ && exit 1)

Makefile: $(SRC)/Makefile.in config.status
	./config.status

#=== INSTALLATION, UNINSTALLATION, CLEANING ===
clean:
	rm -rf release
	rm -f apt_untangle_configured.py
ifneq "$(IS_RELEASE)" "True"
	rm -rf man-compiled
endif

distclean: clean
	rm -f Makefile
	rm -f config.status

maintainer-clean: distclean
	@echo 'This command is intended for maintainers to use; it'
	@echo 'deletes files that may need special tools to rebuild.'
	@echo "If you've run it by mistake, you'll have to unpack "
	@echo "the distribution archive again."
	rm -f release.json
	rm -rf man-compiled

ifeq "$(ENABLE_DOC)" "True"
install: apt_untangle_configured.py install-doc
else
install: apt_untangle_configured.py
	#Installation of docs is disabled
endif
	install -D -m 555 apt_untangle_configured.py $(BINDIR)/$(PROGRAM_NAME)

uninstall: uninstall-doc
	rm -f $(BINDIR)/$(PROGRAM_NAME)
	rmdir --parents --ignore-fail-on-non-empty $(BINDIR)

ifeq "$(COMPRESS_MAN)" "True"
install-doc: man-compiled/$(DEFAULT_MAN_LANGUAGE)/$(PROGRAM_NAME).$(MAN_SECTION).gz
else
install-doc: man-compiled/$(DEFAULT_MAN_LANGUAGE)/$(PROGRAM_NAME).$(MAN_SECTION)
endif
	install -m 755 -d $(DEFMANDIR)
	install -m 644 -t $(DEFMANDIR) $<

uninstall-doc:
	rm -f $(DEFMANDIR)/$(PROGRAM_NAME).$(MAN_SECTION) $(DEFMANDIR)/$(PROGRAM_NAME).$(MAN_SECTION).gz
	rmdir --parents --ignore-fail-on-non-empty $(DEFMANDIR)

#=== COMPILING DOCUMENTATION ===
ifeq "$(COMPRESS_MAN)" "True"
doc: man-compiled/$(DEFAULT_MAN_LANGUAGE)/$(PROGRAM_NAME).$(MAN_SECTION) man-compiled/$(DEFAULT_MAN_LANGUAGE)/$(PROGRAM_NAME).$(MAN_SECTION).gz
else
doc: man-compiled/$(DEFAULT_MAN_LANGUAGE)/$(PROGRAM_NAME).$(MAN_SECTION)
endif

man-compiled/$(DEFAULT_MAN_LANGUAGE)/$(PROGRAM_NAME).$(MAN_SECTION): apt-untangle.$(MAN_SECTION).$(DEFAULT_MAN_LANGUAGE).w
ifdef PRECOMPILED_MAN
	echo man pages were precompiled, cannot rebuild
	echo To recompile them, do \"make maintainer-clean\", then re-\"configure\" and re-\"make\"
	echo (this is not recommended on numbered releases)
	exit 1
else
	mkdir -p man-compiled/$(DEFAULT_MAN_LANGUAGE)
	cp $< man-compiled/$(DEFAULT_MAN_LANGUAGE)/$(PROGRAM_NAME).$(MAN_SECTION).w
	#a2x can take a while
	a2x -f manpage man-compiled/$(DEFAULT_MAN_LANGUAGE)/$(PROGRAM_NAME).$(MAN_SECTION).w
	rm man-compiled/$(DEFAULT_MAN_LANGUAGE)/$(PROGRAM_NAME).$(MAN_SECTION).w
endif

%.gz: %
	gzip -fk $<

#=== TESTING ===
test_data:
	#To run tests you need an additional test data package
	#It's about 21 MB to download, about 131 MB after unpacking
	#Run "make download-test-data" to download it automatically
	#(it uses curl unless you set DOWNLOADER as described in the Makefile)
	#or download it manually at
	@echo $(TEST_DATA_URL)
	#or look for it at
	@echo $(DOWNLOADS_URL)
	exit 1

download-test-data: apt-untangle_test_data.tar.xz
	sha256sum -c $(SRC_ESC)/test_data_dist.sha256 || \
	(echo Bad checksum, try \"make --always-make download-test-data\" to redownload && exit 1)
	tar -xf apt-untangle_test_data.tar.xz

apt-untangle_test_data.tar.xz:
	$(DOWNLOADER) $(TEST_DATA_URL) || \
	(echo Error while downloading, removing incomplete file; rm $@; exit 1)

check: check-script.sh test_data apt_untangle_configured.py
	#make check does require zsh
	zsh $(SRC_ESC)/check-script.sh do-run-test python3

#=== RELEASES ===
RELEASEDIR := release/$(PROGRAM_NAME)-$(VERSION)
dist: doc
ifdef AUTO_VERSION
	@echo "VERSION is set to default, can't make dist"
	exit 1
else
	mkdir -p $(RELEASEDIR)
	install -m 755 -t $(RELEASEDIR) apt_untangle.py check-script.sh configure configure.py
	install -m 644 -t $(RELEASEDIR) apt-untangle.$(MAN_SECTION).$(DEFAULT_MAN_LANGUAGE).w Makefile.in README.rst LICENSE.txt test_data_dist.sha256
	#Compressed man pages won't be in the release, they will be compressed on the user's computer
	find man-compiled -type f -name '*.$(MAN_SECTION)' -exec install -D -m 644 '{{}}' $(RELEASEDIR)/'{{}}' \;
	echo '{{ "version": "$(VERSION)", "man_compiled": true }}' > $(RELEASEDIR)/release.json
	tar --numeric-owner --group=0 --owner=0 --xz --directory release -cvf $(PROGRAM_NAME)-$(VERSION).tar.xz $(PROGRAM_NAME)-$(VERSION)
	chmod 644 $(PROGRAM_NAME)-$(VERSION).tar.xz
	rm -r $(RELEASEDIR)
	rmdir --ignore-fail-on-non-empty release
endif