APT-UNTANGLE(8)
===============
:doctype: manpage

== NAME
apt-untangle - disable an APT repository, upgrading, downgrading or removing packages from it

== SYNOPSIS
*apt-untangle* ['OPTIONS'] [*--untangle 'REPOSITORY'*] [*--untangle-unavailable*] [[*--*] 'PACKAGES']

== DESCRIPTION
apt-untangle disables an APT repository and upgrades, downgrades or (if you enable it) removes packages from it, unless the exact same version is available in another repository. It can also work on packages that aren\'t available in any repository (useful if you e.g. already disabled a repository).

apt-untangle must be run with either *--untangle-unavailable* or at least one *--untangle 'REPOSITORY'* (you may use both and you may use use multiple *--untangle* options for multiple repositories). If you specify 'PACKAGES', only those packages will be untangled (i.e. replaced with a different version or removed), otherwise all packages from all repositories will be.

See <<Untangling,What to Untangle>> for more info, <<Packages,Specifying Packages and Sections>> on how to specify packages and sections (although you can simply use package names) and <<Repos,Specifying Repositories>> on how to specify repositories (long story short, deb strings).

By default apt-untangle will upgrade packages or, if there is no newer version, downgrade them. If neither a newer nor an older version is available, apt-untangle will skip a package.

Once apt-untangle has decided what to do, it will call *aptitude*(8) to perform the actions. It will be called with --prompt, so you will definitely be asked for confirmation. You can make apt-untangle something else instead, see <<Backend Options>.

*WARNING: the default mode of operation has not been implemented yet. It must be run with --print-aptitude-command. Also you should run* apt-get update *or* aptitude update *before running apt-untangle.*

== OPTIONS

[[Untangling]]
=== What to Untangle
*--untangle 'REPOSITORY'*::
    Untangle packages from this repository. See <<Repos,Specifying Repositories>> on what 'REPOSITORY' can be. You may repeat this option like *--untangle 'REPOSITORY1'* *--untangle 'REPOSITORY2'* to untangle multiple repositories at once. If the chosen backend is *--aptitude* (it\'s the default), selected repositories will be disabled before calling aptitude.

*--untangle-unavailable*::
    Untangle packages that aren\'t available in any repository.

*'PACKAGES'*::
    If you only want certain packages to be untangled, specify their names on the command line. In the unlikely event that you have packages whose names begin with all packages names, put *--* before the packages to end option processing, like this: *-- -weird-package --weird-package-common*. If you don\'t specify any package names, then all packages from selected repositories (or, with *--untangle-unavailable*, those not available in any repositories) will be untangled. See <<Packages,Specifying Packages and Sections>> on what 'PACKAGES' can be.

[[Filters]]
=== Additional Filters
With these options you can opt to skip some packages that would otherwise have to be untangled or ignore an entire repository. These options will not be communicated to the backend (e.g. aptitude).

*--ignore 'REPOSITORY'*::
    Consider this repository non-existent when untangling packages, but do not actively untangle any packages from it.

*--skip-package 'PACKAGE'*::
    Skip this package. Can be used more than once.

*--skip-section 'SECTION'*::
    Skip this section. Can be used more than once.

*--skip-self*, *--no-touch-self*::
    Skip the package that contains apt-untangle itself. The package name is hardcoded, run *apt-untangle --version* to discover it, but it\'s most likely *apt-untangle*. By default it doesn\'t get any special treatment.

[[Actions]]
=== Actions
These are the actions that apt-untangle will try to take. They must be specified *in the desired order*, because apt-untangle will try the first action for each package, then, if it\'s impossible, the second and so on. The default is *--upgrade --downgrade*.

To simplify scripting, actions can be repeated and --remove does not have to be the last action in a row, but it\'s up to

*--upgrade*::
    Upgrade package, if there is a newer version available.

*--upgrade-all*::
    If all packages that need untangling can be upgraded, upgrade them, otherwise skip to next action.

*--downgrade*::
    Downgrade package, if there is an older version available.

*--downgrade-all*::
    If all packages that need untangling can be downgraded, downgrade them, otherwise skip to next action.

*--remove*::
    Remove package. This is always possible, so if you specify *--remove*, it should probably be the last action in the row, but apt-untangle even if it isn\'t. If you don\'t specify *--remove*, packages that can\'t otherwise be untangled will be skipped.

*--skip*::
    Skip package, ignore all actions after this one. This action exists only to simplify scripting.

[[Auto]]
=== Marking Packages as Automatically Installed
Whether to mark packages as manually or automatically installed.

*--preserve-auto*::
    Check what packages are marked as automatically installed and preserve the mark (this is the default).

*--all-manual*::
    Mark all untangled packages as manually installed.

*--all-auto*::
    Mark all untangled packages as automatically installed. Note that if nothing depends on an automatically installed package, aptitude can decide to immediately remove it after installation.

[[Backend]]
=== Backend Options
What should apt-untangle do to actually perform the planned actions? 

*--aptitude*::
    Run *aptitude*. This is the default.

*--print-aptitude-command*::
    Print the *aptitude* command to untangle your system, but do not actually run it. In this case apt-untangle doesn\'t require root and won\'t update package lists before running.

[[Other]]
=== Other options
*--no-check-root*::
    Do not check if apt-untangle is running as root. It is up to you to make aptitude work without superuser privileges. With --print-aptitude-command superuser privileges are not required and therefore not checked.

*--no-prompt*::
	Do not add --prompt to the aptitude command

*--sysroot 'DIRECTORY'*::
    Use this directory as the system root instead of */*	

[[Special]]
=== Special Modes
*-h, --help*::
    Print a help message and exit.

*--version*::
    Print version information and exit.

*--license*::
    Print information about the license of the program and exit.

[[Packages]]
== Specifying Packages and Sections
The easiest way to specify a package or a section wherever required is to write its name, like this: *gcc* or *libjson0*.

This is currently the only way to specify packages. In the future apt-untangle will parse architectures (like *gcc:amd64*), allow *&#42;* (like *libjson&#42;*) and full Python regular expressions.

[[Repos]]
== Specifying Repositories
Whenever you need to specify a repository, write the corresponding deb string (see *sources.list*(5)), e.g.:

*deb http://ppa.launchpad.net/xorg-edgers/ppa/ubuntu trusty main*

Comments are processed properly. Options in deb strings (like *deb [arch=amd64] ftp://ftp.debian.org/debian unstable contrib*) are currently ignored. *deb-src* strings will also be ignored.

RFC822 format is not allowed on the command line, because hardly anyone would want to use it anyway.

In the future we are planning to allow PPA specifications like this:

*ppa:xorg-edgers/ppa*

but currently they are not processed, write the full string instead:

[[Exit]]
== EXIT STATUS
*0* = no errors, untangling complete
*1* = incorrect command line arguments
*2* = there was nothing to untangle. *Note that this means still the program has worked correctly.*

Other non-zero exit codes indicate errors, but they are not set in stone yet (well, since apt-untangle is in early development, nothing is, but still...)

[[Examples]]
== EXAMPLES
*apt-untangle --untangle-unavailable --print-aptitude-command*

Print aptitude command to upgrade or downgrade all packages that aren\'t available in any repository. Packages that can\'t be upgraded or downgraded will be skipped.

*apt-untangle --print-aptitude-command --untangle "deb http://ppa.launchpad.net/xorg-edgers/ppa/ubuntu trusty main" --upgrade --downgrade --remove*

Print aptitude command to upgrade, downgrade or remove - in that order of preference - all packages from the xorg-edgers PPA (on Ubuntu 14.04 Trusty Tahr).

[[SeeAlso]]
== See Also
*aptitude*(8), *apt-get*(8), *apt*(8), *sources.list*(5), *ppa-purge*(1)

[[Author]]
== Author
apt-untangle was written by Fedor Uvarov. Project page: 'https://bitbucket.org/Acharvak/apt-untangle'. Bug reports and feature requests should be submitted at 'https://bitbucket.org/Acharvak/apt-untangle/issues'. At the time of writing a Bitbucket account was NOT required, anonymous submissions were accepted.