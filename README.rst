============
apt-untangle
============

MAINTENANCE NOTICE
==================
As of May 2020, I haven't worked on this project for quite some time because I haven't had any need to use it recently. If I decide to work on it again, I'll probably transfer it to my GitHub profile (https://github.com/Acharvak). The first order of business will be to migrate to python-apt instead of working with Aptitude and its text output (the process has already begun in the branch "rewrite-with-python-apt").

In the meantime, since Bitbucket is dropping support for Mercurial, I've already switched to Git on Bitbucket and copied all the downloads over (although the released versions are behind the master branch). The code should remain available for the time being.

The rest of the README has not been updated for years.

What is apt-untangle
====================
**apt-untangle(8) is an auxiliary program for APT-based GNU/Linux distributions (Debian, Ubuntu, Linux Mint etc.). It lets you disable an APT repository, upgrading, downgrading or removing packages from it.**

For example, if you use Ubuntu or its derivative, like Mint, if you add a Personal Package Repository (PPA) to your system (like xorg-edgers_) and then decide to remove it and revert your system to its previous state, you can use apt-untangle to remove it. In case of xorg-edgers on Ubuntu 14.04 Trusty Tahr or Linux Mint 17 (based on *trusty*), you can do it like this::

   COMMAND=$(apt-untangle --untangle "deb http://ppa.launchpad.net/xorg-edgers/ppa/ubuntu trusty main" --print-aptitude-command)
   #Disable xorg-edgers PPA in /etc/apt/sources.list or /etc/apt/sources.list.d
   #you may want to use the Software Sources management application in your distribution
   sudo aptitude update
   sudo $COMMAND #in zsh: sudo ${=COMMAND} 

*Yeah, I know, it's sort of unwieldy right now, because apt-untangle is in an early stage of development and many things have not been implemented yet. The finished version should be able to understand the string ppa:xorg-edgers/ppa just as well and it should be able to disable the repository on its own. See* `Roadmap`_ *for more information.*

I should probably make this clear:

**apt-untangle is currently in an early stage of development. Use at your own risk. But currently it can't change anything in your system, it can only print the proper command to carry out all actions with aptitude, with --prompt parameter at that. You will definitely have a chance to review all changes before they are made.**

.. _xorg-edgers: https://launchpad.net/~xorg-edgers/+archive/ubuntu/ppa

How is it better than ppa-purge
===============================
ppa-purge_ is another popular script, similar to apt-untangle. However, apt-untangle already has a number of advantages over ppa-purge and will have more in the future:

* apt-untangle can disable any APT repository, not only a PPA
* apt-untangle can be made to work only on certain packages, or skip certain packages, or skip certain sections
* apt-untangle can be used *after* you've disabled a PPA (with ``--untangle-unavailable``). Make sure you don't downgrade any packages by mistake though.
* apt-untangle is safer by default: it won't remove packages, only upgrade or downgrade them and skip packages where neither action is possible. But if you want, you can select exactly what actions (``--upgrade``, ``--downgrade`` or ``--remove``) are permitted and in what order of preference.
* *[Planned]* apt-untangle will restore a disabled repository in case of errors while removing packages.
* *[Planned]* apt-untangle will be able to use Synaptic as a backend, not only aptitude

.. _ppa-purge: https://launchpad.net/ppa-purge

License
=======
apt-untangle is licensed under Apache License 2.0, see LICENSE.txt for details.

Getting it
==========
Download a released version here:

https://bitbucket.org/Acharvak/apt-untangle/downloads
   
You'll probably want the latest numbered version. Please note that you **don't** need the file ``apt-untangle_test_data.tar.xz`` unless you want to run ``make check``.

If you want the bleeding edge version, clone the Mercurial repository:

   hg clone https://bitbucket.org/Acharvak/apt-untangle
   
The main website is here:

https://bitbucket.org/Acharvak/apt-untangle
   
Prerequisites
=============
To use the program, you will need:

* Python 3.4+
* aptitude 0.6.8.2+

To install it from this source, you'll need:

* Python 3.4+
* GNU Make 3.81+
* **[optional]** AsciiDoc 8.6.9+, “a2x -f manpage” must work correctly
* **[optional]** zsh 5.0.2+

Actually older versions of Python, aptitude and make (especially aptitude and make) may be enough. These are just the tested versions.

AsciiDoc is only required to compile the man page, because it's written in AsciiDoc, not *troff* (the actual language of man pages). But it's only needed if you're using the Mercurial repository directly, released versions (which are available at https://bitbucket.org/Acharvak/apt-untangle/downloads) include precompiled man pages.

zsh is only required to run ``make check``.

Installation
============
**WARNING: version 0.1.1 uses an older procedure than this here. Refer to the included README.rst**

First, run::

   ./configure
   
This will configure the script for installation into /usr/local. To install into another directory, instead of just ``./configure`` run::

   ./configure --prefix=/usr #To install into /usr
   
./configure is NOT an autoconf script (although to be easy to use it provides a similar interface). It requires python3 to be in your $PATH. If you don't have python3 in $PATH, run::

   ./configure PYTHON=/path/to/python3 --python-shebang=/path/to/python3

There are also other options you can set (but most likely won't need to), to get a list, run::

   ./configure --help

Then, once you've configured the program, run::

   make

This will produce a prepared version of the script and compile and/or compress the documentation. To install the program, run::

   sudo make install
   
Or, if you're familiar with CheckInstall_ (`read on Wikipedia`_) and have it installed, you can run::

   sudo checkinstall

.. _CheckInstall: http://asic-linux.com.mx/~izto/checkinstall/
.. _read on Wikipedia: https://en.wikipedia.org/wiki/CheckInstall

Using apt-untangle
==================
See ``man apt-untangle`` or the file ``apt-untangle.8.en.w``.

Uninstalling
============
If you've installed apt-untangle via ``make install``, to uninstall it run::

   sudo make uninstall

If you've installed it using CheckInstall, it has been installed as a package. Remove this package. Most likely you will need to run::

   sudo aptitude remove apt-untangle
   
or::

   sudo apt-get remove apt-untangle

Roadmap
=======
The following features are planned for the 1.0 release. They are listed in the planned order of implementation, although we can't promise any specific days:

1. **[ABANDONED]** Switch to docopt_ instead of ``argparse`` for parsing command line arguments. This could mean a change in the syntax of ``apt-untangle``.
#. **[DONE]** Add short option equivalents.
#. **[DONE]** Add --no-prompt.
#. **[Most likely]** Rewrite the man page source in reStructuredText instead of AsciiDoc.
#. Implement a procedure to build deb packages, create an Ubuntu PPA.
#. Have apt-untangle read source lists and disable the repository (or at least make sure it is enabled) and actually call aptitude.
#. Enable interpretation of ``ppa:user/archive`` lines.
#. **[Maybe]** Lock the lock files properly.
#. Work with Synaptic.
#. Enable internationalization using gettext (apt-untangle is written with gettext in mind, but it is not enabled nor tested yet).

.. _docopt: http://docopt.org/

Contacting the author, reporting bugs
=====================================
apt-untangle was written by Fedor Uvarov. Bug reports and feature requests should be submitted to `our Bitbucket issue tracker`_. A Bitbucket account is NOT required, anonymous submissions are accepted (or at least it was so at the time of writing).

.. _our Bitbucket issue tracker: https://bitbucket.org/Acharvak/apt-untangle/issues

Patches can be sent as pull requests on Bitbucket or, if you really don't want to send pull requests, create an issue in the issue tracker and add your patch as an attachment in a commonly used patch format, or send the patch per e-mail.

If you need to include information that should not be disclosed to the public or if you otherwise absolutely need to contact the author by e-mail, send it to acharvak AT fsfe.org
