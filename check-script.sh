#!/bin/zsh
set -e
if [[ $1 != 'do-run-test' ]]; then
	echo "$0 should only be run via \"make check\""
	exit 1
fi

PYTHON=$2
RUN=($PYTHON apt_untangle_configured.py --sysroot=test_data --print-aptitude-command)

do_test() {
	echo "> Testing:" ${(q-)TEST}
	if [[ -z $EXPECTED ]]; then
		echo "> Expecting no output"
	else
		echo "> Expecting:" ${(q-)EXPECTED}
	fi
	if [[ -n $1 ]]; then
		echo "> Expecting exit code:" $1
		expcode=$1
	else
		expcode=0
	fi
	resultcode=0
	result=($($RUN $TEST)) || resultcode=$? 
	if [[ -z $result ]]; then
		echo "> Got no output"
	else
		echo "> Got:" $result
	fi
	echo "> Exit code:" $resultcode
	#For some reason (a bug?) if result=(1 2 3 4) and EXPECTED=(1 2 3)
	#${(oz)result} == ${(oz)EXPECTED} is true. But this way it should work.
	result_sorted=${(oz)result}
	expected_sorted=${(oz)EXPECTED}
	if [[ $result_sorted == $expected_sorted && $resultcode -eq $expcode ]]; then
		echo "> PASSED\n"
	else		
		echo "> FAILED\n"
		exit 2
	fi
}

echo ">>> Basic test on one package"
TEST=(--untangle "deb http://ppa.launchpad.net/xorg-edgers/ppa/ubuntu trusty main" libglamor0)
EXPECTED=(aptitude install --prompt libglamor0:amd64=0.6.0-0ubuntu4+)
do_test

echo ">>> Basic test on all packages"
TEST=(-X)
EXPECTED=(aptitude install --prompt libtsan0:amd64=4.8.2-19ubuntu1+ libgomp1:amd64=4.8.2-19ubuntu1+ cpp:amd64=4:4.8.2-1ubuntu6+ libquadmath0:amd64=4.8.2-19ubuntu1+ libitm1:amd64=4.8.2-19ubuntu1+ libgfortran3:amd64=4.8.2-19ubuntu1+ libatomic1:amd64=4.8.2-19ubuntu1+ gcc-4.9-base:amd64=4.9.1-0ubuntu1+ gcc-4.9-base:i386=4.9.1-0ubuntu1+ gcc:amd64=4:4.8.2-1ubuntu6+ libgcc1:amd64=1:4.9.1-0ubuntu1+ libgcc1:i386=1:4.9.1-0ubuntu1+ libstdc++6:amd64=4.8.2-19ubuntu1+ libstdc++6:i386=4.8.2-19ubuntu1+ mint-meta-cinnamon:all+)
#7 packages will remain tangled
do_test

echo ">>> Testing --all-auto"
TEST=(--untangle-unavailable --all-auto cpp)
EXPECTED=(aptitude install --prompt cpp:amd64=4:4.8.2-1ubuntu6+M)
do_test

echo ">>> Testing multiple repositories"
TEST=(--untangle "deb http://archive.ubuntu.com/ubuntu trusty main restricted universe multiverse" --untangle "deb http://archive.ubuntu.com/ubuntu trusty-updates main restricted universe multiverse" xserver-xorg-video-intel)
EXPECTED=(aptitude install --prompt xserver-xorg-video-intel:amd64+M)
do_test

echo ">>> Testing mixed long/short options, --all-manual"
TEST=(-x "deb http://archive.ubuntu.com/ubuntu trusty main restricted universe multiverse" --untangle "deb http://archive.ubuntu.com/ubuntu trusty-updates main restricted universe multiverse" --all-manual xserver-xorg-video-intel)
EXPECTED=(aptitude install --prompt xserver-xorg-video-intel:amd64+)
do_test

echo ">>> Testing comments"
TEST=(-x "deb http://extra.linuxmint.com rebecca main #Test comments" -dr)
EXPECTED=(aptitude install --prompt libdvdcss2:amd64- gstreamer0.10-ffmpeg:amd64-)
do_test

echo ">>> Testing --skip-package"
TEST=(--untangle "deb http://extra.linuxmint.com rebecca main" --skip-package libdvdcss2 --downgrade --remove)
EXPECTED=(aptitude install --prompt gstreamer0.10-ffmpeg:amd64-)
do_test

echo ">>> Testing --no-prompt"
TEST=(-x "deb http://extra.linuxmint.com rebecca main" --skip-package libdvdcss2 --no-prompt -dr)
EXPECTED=(aptitude install gstreamer0.10-ffmpeg:amd64-)
do_test

echo ">>> Testing deb-src (should be ignored)"
TEST=(-x "deb http://extra.linuxmint.com rebecca main" -x "deb-src http://extra.linuxmint.com rebecca main" --skip-package libdvdcss2 --no-prompt -dr)
EXPECTED=(aptitude install gstreamer0.10-ffmpeg:amd64-)
do_test

echo ">>> Testing --skip-section"
TEST=(-rx "deb http://extra.linuxmint.com rebecca main" --skip-section libs)
EXPECTED=()
do_test 2

echo "> ALL TESTS PASSED"
