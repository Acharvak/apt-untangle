#!/usr/bin/env python3
#apt-untangle - disable an APT repository and properly downgrade/upgrade/remove packages

#Copyright 2015 Fedor Uvarov
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

import argparse
import os
import re
import sys
import string

PROGRAM_NAME = 'apt-untangle'
PROGRAM_WEBSITE = 'https://bitbucket.org/Acharvak/apt-untangle'
PROGRAM_COPYRIGHT_YEARS = '2015'

#The next four lines marked with #! are modified by the Makefile during configuration
PROGRAM_VERSION = '' #!
if not PROGRAM_VERSION:
    PROGRAM_VERSION = '<VERSION NOT SET>'
PPA_HOST = '' #!
OWN_PACKAGE_NAME = '' #! Will be used for --no-touch-self

EXIT_OK = 0
EXIT_BAD_ARGS = 1
EXIT_NOTHING_TO_UNTANGLE = 2
EXIT_NOT_ROOT = 3
EXIT_SUBPROCESS_ERROR = 4
EXIT_NO_SOLUTIONS = 5 #There is stuff to untangle, but we have no solutions
EXIT_KEYBOARD_INTERRUPT = 10
EXIT_INTERNAL_ERROR = 100

try:
    raise ImportError('gettext support currenty disabled')
    import gettext
except ImportError:
    #gettext shim
    class gettext:
        def gettext(self, msg):
            return msg
        def ngettext(self, singular, plural, n):
            if n == 1:
                return singular
            else:
                return plural
    gettext = gettext()

def _(msg):
    return gettext.gettext(msg)

class AptUntangleError(Exception):
    """For predictable errors within the program"""
    def __init__(self, msg, returncode):
        super().__init__(msg)
        self.msg = msg
        self.returncode = returncode #Recommended return code
    def __str__(self):
        return str(self.msg)

ASCII_LETTERS = set(string.ascii_letters)
def compare_version_components(ver1, ver2):
    """Returns -1 if ver1 older than ver2, 0 if equal, 1 if ver1 newer than ver2

    This function compares upstream version or Debian version, they must be fed to it
    separately.
    """
    ver1 = re.findall('([^0-9]*)([0-9]*)', ver1)
    ver2 = re.findall('([^0-9]*)([0-9]*)', ver2)
    for i in range(len(ver1)):
        g1_letters = ver1[i][0]
        g1_num = int(ver1[i][1]) if ver1[i][1] else 0
        if i >= len(ver2):
            if len(g1_letters) > 0 and g1_letters[0] == '~':
                return -1
            else:
                return 1
        else:
            g2_letters = ver2[i][0]
            g2_num = int(ver2[i][1]) if ver2[i][1] else 0
            for m in range(len(g1_letters)):
                L1 = g1_letters[m]
                if m >= len(g2_letters):
                    if L1 == '~':
                        return -1
                    else:
                        return 1
                else:
                    L2 = g2_letters[m]
                    if L1 == '~' and L2 != '~':
                        return -1
                    elif L1 != '~' and L2 == '~':
                        return 1
                    elif L1 in ASCII_LETTERS and L2 not in ASCII_LETTERS:
                        return -1
                    elif L1 not in ASCII_LETTERS and L2 in ASCII_LETTERS:
                        return 1
                    elif ord(L1) < ord(L2):
                        return -1
                    elif ord(L1) > ord(L2):
                        return 1
            if len(g2_letters) > len(g1_letters):
                if g2_letters[len(g1_letters)] == '~':
                    return 1
                else:
                    return -1
            if g1_num < g2_num:
                return -1
            elif g1_num > g2_num:
                return 1
    #If the prefix is the same
    if len(ver2) > len(ver1):
        if ver2[len(ver1)][0] and ver2[len(ver1)][0] and ver2[len(ver1)][0][0] == '~':
            return 1
        else:
            return -1
    #If everything is the same
    return 0

VERSION_RE = re.compile('^(?:(?:(?P<epoch>[0-9]+):)?|(?=[^:]*$))(?P<upstream>[-.+:~a-zA-Z0-9]+?)(?:-(?P<debian>[+.~a-zA-Z0-9]*))?$')
def compare_versions(ver1, ver2, package_name):
    """Compares version numbers according to deb-version(5)

    Returns -1 if ver1 older than ver2, 0 if equal, 1 if ver1 newer than ver2
    """
    ver1m = VERSION_RE.fullmatch(ver1)
    ver2m = VERSION_RE.fullmatch(ver2)
    msg = _('Package {Package} has incorrect version: {Version}')
    if not ver1m or ver1m.group('debian') == '':
        raise AptUntangleError(msg.format(Package=package_name, Version=ver1), EXIT_SUBPROCESS_ERROR)
    if not ver2m or ver2m.group('debian') == '':
        raise AptUntangleError(msg.format(Package=package_name, Version=ver2), EXIT_SUBPROCESS_ERROR)

    #Checking epoch
    ver1_version = int(ver1m.group('epoch') or 0)
    ver2_version = int(ver2m.group('epoch') or 0)
    if ver1_version < ver2_version:
        return -1
    elif ver1_version > ver2_version:
        return 1

    #Checking upstream version
    comp = compare_version_components(ver1m.group('upstream'), ver2m.group('upstream'))
    if comp != 0:
        return comp

    #Checking Debian revision (if present)
    ver1_deb = ver1m.group('debian')
    ver2_deb = ver2m.group('debian')
    if ver1_deb is None:
        if ver2_deb is None:
            return 0
        else: #ver2 has a debian revision, ver1 doesn't
            return -1
    else:
        if ver2_deb is None:
            return 1
        else:
            return compare_version_components(ver1_deb, ver2_deb)

EMPTYLINE_RE = re.compile(r'^\s*$')
class Collector:
    """Collects data from package info streams"""
    def __init__(self, fields):
        self.result = {}
        self.regexps = [None] * len(fields)
        for i in range(len(fields)):
            self.regexps[i] = {
                'name': fields[i],
                're': re.compile('^{}: (.*)'.format(re.escape(fields[i]))),
            }
    def process(self, line):
        if line is None or EMPTYLINE_RE.match(line):
            res = self.result
            self.result = {}
            return res
        elif line[0] != ' ': #To save some time on descriptions
            for r in self.regexps:
                m = r['re'].match(line)
                if m:
                    self.result[r['name']] = m.group(1)

#pkg_collector should be reusable, so we create it only once
pkg_collector = Collector(('Package', 'Architecture', 'Version', 'Section', 'Status'))
PKG_INFO_PROBLEM_FMT = _("""Unexpected end of package information in {FileOrStreamName},
{PackageName},
Line: {LineNumber},
{PreviousLine}""")
PKG_NAME_STR = _('Package name:')
UNKNOWN_PKG_STR = _('Unknown package name')
PREVIOUS_LINE_EMPTY_STR = _("Previous line is empty")
def read_packages(stream, stream_name, repo_id, db, query):
    """Read package info from a given stream. Put them correctly into db.

    query must be 'all', 'installed' or 'existing'
    """
    line = 1
    last_line = '' #TODO: make it more pretty or something

    def add_to_db(p):
        if 'Package' not in p or 'Architecture' not in p or 'Version' not in p:
            raise AptUntangleError(PKG_INFO_PROBLEM_FMT.format(
                        FileOrStreamName=stream_name,
                        LineNumber=line,
                        PreviousLine=last_line,
                        PackageName=PKG_NAME_STR + str(p['Package']) if 'Package' in p else UNKNOWN_PKG_STR
                    ), EXIT_SUBPROCESS_ERROR)
        pv = {
            'version': p['Version'],
            'repo': repo_id,
            'status': p['Status'] if 'Status' in p else None,
            'section': p['Section'] if 'Section' in p else None,
        }
        db.setdefault(p['Package'], {}).setdefault(p['Architecture'], {}).setdefault('all', []).append(pv)
        if pv['status'] == 'install ok installed':
            db[p['Package']][p['Architecture']]['installed'] = pv
                                   
    for s in stream:
        p = pkg_collector.process(s)
        if p:
            add_to_db(p)
        if not s:
            last_line = PREVIOUS_LINE_EMPTY_STR
        line += 1
    p = pkg_collector.process(None)
    if p:
        add_to_db(p)

#autoselect_collector should also be reusable
#Although there is quitr a bit of copied code here, but generalizations would be
#much more confusing
autoselect_collector = Collector(('Package', 'Architecture', 'Auto-Installed'))
def read_autoinstalled_packages(stream, stream_name, db):
    """Read extended state information (normally /var/lib/apt/extended_states)
    and select autoinstalled packages.
    """
    line = 1
    last_line = '' #TODO: make it more pretty or something
    
    def add_to_db(p):
        if 'Package' not in p or 'Architecture' not in p:
            raise AptUntangleError(PKG_INFO_PROBLEM_FMT.format(
                        FileOrStreamName=stream_name,
                        LineNumber=line,
                        PreviousLine=last_line,
                        PackageName=PKG_NAME_STR + str(p['Package']) if 'Package' in p else UNKNOWN_PKG_STR
                    ), EXIT_SUBPROCESS_ERROR)
        if p['Package'] in db:
            if p['Architecture'] in db[p['Package']]:
                db[p['Package']][p['Architecture']]['auto'] = ('Auto-Installed' in p and p['Auto-Installed'] != '0')

    for s in stream:
        p = autoselect_collector.process(s)
        if p:
            add_to_db(p)
        if not s:
            last_line = PREVIOUS_LINE_EMPTY_STR
        line += 1
    p = autoselect_collector.process(None)
    if p:
        add_to_db(p)

#Root must be prepended to all these in main()
FileStatus = 'var/lib/dpkg/status'
DirRepoLists = 'var/lib/apt/lists'
FileExtStatus = 'var/lib/apt/extended_states'

def set_sysroot(root):
    global FileStatus, DirRepoLists, FileExtStatus
    FileStatus = root + FileStatus
    DirRepoLists = root + DirRepoLists
    FileExtStatus = root + FileExtStatus

def gather_data(untangle_unavailable, untangle_repos, ignore_repos, untangle_package_names,
                skip_package_names, skip_sections, need_auto):
    """Gathers information required to plan untangling"""
    
    #Collecting all installed packages
    packages = {}
    with open(FileStatus) as f:
        read_packages(f, FileStatus, None, packages, 'installed')

    #Assigning repositories to each of them
    dirname = DirRepoLists
    targets = os.listdir(dirname)
    for fname in targets:
        m = re.match('^(.*?)(?:_binary-[a-zA-Z0-9]+)?_Packages$', fname)
        if m:
            repo = m.group(1)
            if repo not in ignore_repos:
                with open(dirname + "/" + fname) as f:
                    read_packages(f, fname, repo, packages, 'existing')
    targets = None #Just for the garbage colector

    #Reading extended states
    if need_auto:
        with open(FileExtStatus) as f:
            read_autoinstalled_packages(f, FileExtStatus, packages)

    def get_repo_disposition(v):
        """
        Return None if this version is to be ignored,
        False if it's to be untangled,
        True if it's good.
        """
        if v['repo'] is None:
            if untangle_unavailable:
                return False
        elif v['repo'] in untangle_repos:
            return False
        elif v['repo'] not in ignore_repos:
            return True

    #Untangling
    all_packages_upgradeable = True
    all_packages_downgradeable = True
    solutions = []
    for pname, p in packages.items():
        version_possibly_tangled = True #By default it may be tangled
        
        #Maybe it is to be skipped?
        if skip_package_names:
            for pname_re in skip_package_names:
                if pname_re.match(pname):
                    version_possibly_tangled = False
                    break
    
        #If only specific packages need to be untangled, check if it's one of them
        if version_possibly_tangled and untangle_package_names: 
            for pname_re in untangle_package_names:
                if pname_re.match(pname):
                    break
            else:
                version_possibly_tangled = False            

        #Now to check if it's actually tangled and untangle it if necessary
        if version_possibly_tangled:
            if untangle_unavailable:
                bad_repository = True
            else:
                bad_repository = False
            for archname, arch in p.items():
                if 'installed' in arch:
                    #Should we skip it by section?
                    for section_re in skip_sections: #skip_Sections should be an iterable in any case
                        if arch['installed']['section'] and \
                                section_re.match(arch['installed']['section']):
                            break
                    else:
                        best_previous_version = None
                        can_upgrade = False
                        for v in arch['all']:
                            rd2 = get_repo_disposition(v)
                            if rd2 == False:
                                bad_repository = True
                            elif rd2:
                                verdiff = compare_versions(arch['installed']['version'], v['version'], pname)
                                if verdiff > 0: #installed version is newer
                                    if best_previous_version is None or compare_versions(v['version'], best_previous_version['version'], pname) > 0:
                                        best_previous_version = v
                                elif verdiff == 0:
                                    break #It isn't tangled
                                else:
                                    can_upgrade = True
                        else:
                            if bad_repository: 
                                if best_previous_version is None:
                                    all_packages_downgradeable = False
                                if not can_upgrade:
                                    all_packages_upgradeable = False
                                solutions.append({
                                                  'name': pname,
                                                  'arch': archname,
                                                  'best_previous_version': best_previous_version['version'] if best_previous_version else None,
                                                  'can_upgrade': can_upgrade,
                                                  'auto': arch['auto'] if 'auto' in arch else False,
                                                  })
                        
    #Returning the data
    return solutions, all_packages_upgradeable, all_packages_downgradeable

SUB_RE = re.compile(r'\s+|[^a-zA-Z0-9._-]')
def parse_deb_string(s):
    """Predicts the name of the package list, to be used as repository ID
    
    Returns two objects.
    
    First - variable type: a list of strings = the IDs, None = s is an empty string or
    contains only a comment, False = this is a deb-src line.
    
    Second: True or False, whether there were any parameters.
    """
    err = AptUntangleError(_('Invalid package string: {AptSourcesString}').format(AptSourcesString=s),
                           EXIT_INTERNAL_ERROR) #Return code should be replaced
    s = re.match(r'^\s*(.*?)\s*(?:#.*)?$', s).group(1) #Group 1 should definitely contain at least an empty match
    if s.startswith('deb-src'):
        return False, None
    elif not s.startswith('deb'):
        raise err
    
    #Stripping options
    m = re.fullmatch(r'deb\s*(\[[^\]]*])?\s*(\S+)\s*(.*)?', s)
    has_params = not not m.group(1)
    base = m.group(2)
    components = m.group(3)
    
    #Parsing the base
    m = re.fullmatch(r'(?:https?|ftp)://(.*?)(?::\d+)?(/.+)?', base)
    if m:
        base = m.group(1)
        if m.group(2):
            base += m.group(2)
    else:
        base = re.match(r'^(?:\w+:)?(.*)', base).group(1)
    base = SUB_RE.sub('_', base)

    #The result
    result = []
    components = re.split(r'\s*', components)
    if len(components) < 2:
        raise err
    for c in components[1:]:
        result.append('{}_dists_{}_{}'.format(base, SUB_RE.sub('_', components[0]), SUB_RE.sub('_', c)))
    
    return result, has_params
    
#TODO: subclass argparse.ArgumentParser to make it translatable
#or maybe switch to docopt?
#TODO: remove extraneous descriptions
REPOSITORY_STR = _('REPOSITORY_STR')
def prepare_task():
    """Parses the arguments and prepares the task description.
    
    It will process certain situations on its own. If it returns a
    number, that's the suggested exit code.
    """
    parser = argparse.ArgumentParser(
            description=_('Disable an APT repository and properly downgrade/upgrade/remove packages'),
            usage=_('%(prog)s [OPTIONS] [--untangle REPOSITORY_STR] [--untangle-unavailable] [[--] package names]'),
            add_help=False)
    
    group = parser.add_argument_group(_('What to untangle'), _('You MUST use at least one of these arguments'))
    group.add_argument('-X', '--untangle-unavailable', action='store_true',
                       help=_("untangle packages that aren't in any repository"))
    group.add_argument('-x', '--untangle', action='append', metavar=REPOSITORY_STR,
                       help=_("""untangle packages from this repository. Can be used 
more then once to specify multiple repositories. 
REPOSITORY_STR must be either a sources.list entry or a 
ppa:owner/name line"""))
    
    group = parser.add_argument_group(_('Additional filters (every option can be used more than once)'))
    group.add_argument('--disable', action='append', metavar=REPOSITORY_STR,
                       help=("""disable this repository before calling aptitude in the 
end but re-enable it afterwards. Do not untangle 
packages from this repository. Cannot be used with 
--print-aptitude-command."""))
    group.add_argument('--ignore', action='append', metavar=REPOSITORY_STR,
                       help=_("""consider this repository non-existent when deciding 
what to do, but do not untangle any packages from it 
and don't disable it. Note that aptitude can decide to 
install packages from it."""))
    group.add_argument('--skip-package', action='append', metavar=_('PACKAGE_NAME'),
                       help=_("skip this package, do not untangle it no matter what"))
    group.add_argument('--skip-section', action='append', metavar=_('SECTION_NAME'),
                       help=_("skip all packages in this section"))
    
    group = parser.add_argument_group(_('Actions (specify in desired order)'),
                                      description=_('The default is {DefaultActions}').format(DefaultActions='--upgrade --downgrade'))
    group.add_argument('-u', '--upgrade', dest='actions', action='append_const', const='upgrade',
                       help=_('upgrade packages when possible'))
    group.add_argument('--upgrade-all', dest='actions', action='append_const', const='upgrade-all',
                       help=_("""upgrade packages, if every package can be upgraded, otherwise 
skip to the next action"""))
    group.add_argument('-d', '--downgrade', dest='actions', action='append_const', const='downgrade',
                       help=_("""downgrade packages when a previous version is in an 
acceptable repository"""))
    group.add_argument('--downgrade-all', dest='actions', action='append_const', const='downgrade-all',
                       help=_("downgrade packages if every package can be downgraded"))
    group.add_argument('-r', '--remove', dest='actions', action='append_const', const='remove',
                       help=_("""remove packages. Without this option (including by default) packages
that can't be processed otherwise will be skipped. This should be the last option specified, although
it doesn't have to be."""))
    
    group = parser.add_argument_group(_('Marking packages as auto-installed (mutually exclusive)'))
    group.add_argument('--preserve-auto', dest='auto', action='store_const', const=None,
                       help=_("""check which packages are marked as automatically 
installed and preserve the mark (this is the default 
behavior)"""))
    group.add_argument('--all-manual', dest='auto', action='store_false',
                       help=_("mark all packages that are going to be affected as manually installed."))
    group.add_argument('--all-auto', dest='auto', action='store_true',
                       help=_("""mark all packages that are going to be affected as 
automatically installed. Note that aptitude can decide 
to remove them immediately if nothing depends on them."""))

    group = parser.add_argument_group(_('How to untangle in the end (mutually exclusive)'))
    group.add_argument('--aptitude', dest='backend', action='store_const', const='aptitude',
                       help=_("""after deciding what has to be done, run aptitude to 
perform all the actions. This is the default."""))
    group.add_argument('-p', '--print-aptitude-command', dest='backend', action='store_const', const='print-aptitude',
                       help=_("""print the command to call aptitude to untangle the
system. Do not actually call it. With this option the
program doesn't have to be run as root."""))
    
    group = parser.add_argument_group(_('Other options'))
    group.add_argument('--sysroot', help=_("""consider this directory the root of the system instead 
of / (for example, if you're trying to work on a 
mounted system)."""))
    group.add_argument('--no-check-root', action='store_true', help=_("""do not check if this program is run as root. It's up 
to you to make aptitude work in this case. By default 
it will be checked unless you use --print-aptitude-command"""))
    group.add_argument('--no-prompt', action='store_true',
                       help=_("do not add --prompt to the aptitude command"))

    group = parser.add_argument_group('Special modes (incompatible with each other and everything else)')
    group.add_argument('-h', '--help', action='store_true',
                       help=_("show this help and exit"))
    group.add_argument('-V', '--version', action='store_true',
                       help=_("show version information and exit"))
    group.add_argument('--license', action='store_true',
                       help=_("show license information and exit"))

    parser.add_argument('packages', nargs='*', help=argparse.SUPPRESS)
    parser.add_argument('--', nargs=argparse.REMAINDER, dest='packages', action='append', help=argparse.SUPPRESS)

    VersionString = _("""{PROGRAM_NAME} {PROGRAM_VERSION}
    
Copyright {PROGRAM_COPYRIGHT_YEARS} Fedor Uvarov
Distributed under the terms of Apache License 2.0. For more information type
    {ProgramCommand} --license""").format(PROGRAM_NAME=PROGRAM_NAME, ProgramCommand=parser.prog, PROGRAM_VERSION=PROGRAM_VERSION, PROGRAM_COPYRIGHT_YEARS=PROGRAM_COPYRIGHT_YEARS)
    
    LicenseString = _("""{PROGRAM_NAME} {PROGRAM_VERSION}
    
Copyright {PROGRAM_COPYRIGHT_YEARS} Fedor Uvarov
    
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
    
Visit our website:
    {PROGRAM_WEBSITE}""").format(PROGRAM_NAME=PROGRAM_NAME, PROGRAM_VERSION=PROGRAM_VERSION, PROGRAM_COPYRIGHT_YEARS=PROGRAM_COPYRIGHT_YEARS, PROGRAM_WEBSITE=PROGRAM_WEBSITE)

    task = parser.parse_args()
    if task.help:
        parser.print_help()
        return EXIT_OK
    elif task.version:
        print(VersionString)
        return EXIT_OK
    elif task.license:
        print(LicenseString)
        return EXIT_OK
    else:
        #Processing package names
        #TODO: let the user enter regular expressions    
        def name_to_re(lst):
            for i in range(len(lst)):
                lst[i] = re.compile('^{}$'.format(re.escape(lst[i])))
        if task.packages is None:
            task.packages = []
        else:
            name_to_re(task.packages)
        
        if task.skip_package is None:
            task.skip_package = []
        else:
            name_to_re(task.skip_package)
        
        if task.skip_section is None:
            task.skip_section = []
        else:
            name_to_re(task.skip_section)

        #Setting defaults
        if not task.actions:
            task.actions = ['upgrade', 'downgrade']
        if not task.backend:
            task.backend = 'aptitude'

        if not task.untangle_unavailable and not task.untangle:
            print(_("You must specify either --untangle-unavailable or --untangle <repository>"),
                  file=sys.stderr)
            return EXIT_BAD_ARGS

        if not task.sysroot:
            task.sysroot = '/'
        elif task.sysroot[-1] != '/':
            task.sysroot += '/'

        #Returning
        return task

def prepare_aptitude_command(actions, solutions, all_auto, no_prompt):
    parameters = []
    still_tangled = 0
    for s in solutions:
        for a in actions:
            if a == 'upgrade' and s['can_upgrade']:
                parameters.append('{}:{}+{}'.format(s['name'], s['arch'], 'M' if (s['auto'] or all_auto) else ''))
            elif a == 'downgrade' and s['best_previous_version']:
                parameters.append('{}:{}={}+{}'.format(s['name'], s['arch'],
                                                        s['best_previous_version'],
                                                        'M' if (s['auto'] or all_auto) else ''))
            elif a == 'remove':
                parameters.append('{}:{}-'.format(s['name'], s['arch']))
            else:
                continue
            break
        else:
            still_tangled += 1
    if not parameters:
        return None, still_tangled
    else:
        if no_prompt:
            base = 'aptitude install '
        else:
            base = 'aptitude install --prompt '
        return base + ' '.join(parameters), still_tangled

def main():
    try:
        #Preparation
        task = prepare_task()
        if isinstance(task, int):
            exit(task)
        set_sysroot(task.sysroot)
        
        #FIXME: implement --aptitude
        if task.backend == 'aptitude':
            print(_('Calling aptitude directly is not implemented yet, use --print-aptitude-command'))
            exit(EXIT_INTERNAL_ERROR)

        #Parsing --ignore and --untangle
        deb_options = False
        deb_src_ignored = False
        def parse_repos(repo_set, src_list):
            nonlocal deb_options, deb_src_ignored
            for repo in src_list:
                repo_id_list, deb_options_2 = parse_deb_string(repo)
                if repo_id_list == False:
                    deb_src_ignored = True
                if not repo_id_list:
                    continue
                deb_options = deb_options or deb_options_2
                for repo_id in repo_id_list:
                    repo_set.add(repo_id)
            return repo_set
        
        try:
            untangle = parse_repos(set(), task.untangle) if task.untangle else set()
            ignore = parse_repos(set(), task.ignore) if task.ignore else set()
            if deb_options:
                print(_('WARNING: options in deb strings are currently ignored'), file=sys.stderr)
            if deb_src_ignored:
                print(_("WARNING: deb-src string ignored because source repositories do not contain binary packages"), file=sys.stderr)
        except AptUntangleError as e:
            print(e, file=sys.stderr)
            exit(EXIT_BAD_ARGS)
        
        #Searching for solutions (i. e. untangling)
        solutions, all_packages_upgradeable, all_packages_downgradeable = gather_data(task.untangle_unavailable, untangle, ignore, task.packages, task.skip_package, task.skip_section, (task.auto is None))
        if not solutions:
            #TODO: perhaps print messages more tailored to the situation
            #Like, e.g. if there is no package with a passing name, say "no such package"
            print(_('No packages to untangle'), file=sys.stderr)
            exit(EXIT_NOTHING_TO_UNTANGLE)
            
        #Creating the aptitude command
        actions = []
        for a in task.actions:
            if a == 'upgrade-all':
                if not all_packages_upgradeable:
                    continue
                a = 'upgrade'
            elif a == 'downgrade-all':
                if not all_packages_downgradeable:
                    continue
                a = 'downgrade'
            actions.append(a)
            #If the user has, e.g., upgrade-all and upgrade, there will be two upgrades in the line
            #it is up to the user to think how this logic is supposed to work
        if not actions:
            print(gettext.ngettext('There is one package to untangle, but no actions are possible',
                                   'There are {Number} packages to untangle, but no actions are possible',
                                   len(solutions)).format(Number=solutions),
                  file=sys.stderr)
            exit(EXIT_NO_SOLUTIONS)
        aptitude_command, still_tangled = prepare_aptitude_command(actions, solutions, task.auto, task.no_prompt)
        
        #Calling the backend
        if still_tangled:
            still_tangled_msg = gettext.ngettext('One package will remain tangled',
                                                 '{Number} packages will remain tangled',
                                                 still_tangled)
            still_tangled_msg = still_tangled_msg.format(Number=still_tangled)
            print(still_tangled_msg, file=sys.stderr)
        if not aptitude_command:
            print(_('Could not untangle any packages'), file=sys.stderr)
            exit(EXIT_NO_SOLUTIONS)
        if task.backend == 'print-aptitude':
            print(aptitude_command)
            exit(EXIT_OK)
        else:
            print('Not implemented yet', file=sys.stderr)
            exit(EXIT_INTERNAL_ERROR)
    except AptUntangleError as e:
        exit(e.returncode)
    except (FileNotFoundError, PermissionError) as e:
        explanation = None
        isdir = None
        if e.filename == FileStatus:
            explanation = _("""It normally contains status information of all packages in the system.
If it is missing or unreadable, your system may be broken.""")
            isdir = False
        elif e.filename == FileExtStatus:
            explanation = _("""It normally contains information about which packages are automatically installed.
If it is missing or unreadable, your system may be broken, but you can try --all-auto or --all-manual""")
            isdir = False
        elif e.filename == DirRepoLists:
            explanation = _("""It normally contains the lists of packages in every repository.
If it is missing or unreadable, your system may be broken, but you can try running "aptitude update\"""").format(PROGRAM_NAME=PROGRAM_NAME)
            isdir = True
        if isinstance(e, FileNotFoundError):
            if isdir is None:
                msg = _("File or directory not found: {File}")
            elif isdir == False:
                msg = _("File not found: {File}")
            elif isdir:
                msg = _("Directory not found: {File}")
        else:
            if isdir is None:
                msg = _("Couldn't open file or directory due to insufficient permissions: {File}")
            elif isdir == False:
                msg = _("Couldn't open directory due to insufficient permissions: {File}")
            elif isdir == True:
                msg = _("Couldn't open file due to insufficient permissions: {File}")
        print((msg).format(File=e.filename), file=sys.stderr)
        if explanation:
            print(explanation, file=sys.stderr)
        exit(EXIT_SUBPROCESS_ERROR)
    except KeyboardInterrupt:
        print(_('Keyboard interrupt, exiting'), file=sys.stderr)
        exit(EXIT_KEYBOARD_INTERRUPT)
    #Other exceptions will just crash the program
    
    #If we didn't exit until here, there must be an error
    print('Unexpected exit point', file=sys.stderr)
    exit(EXIT_INTERNAL_ERROR)

if __name__ == '__main__':
    main()
    