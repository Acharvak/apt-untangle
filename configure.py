#!/usr/bin/env python3

#Copyright 2015 Fedor Uvarov
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

#NAME AND LATEST NUMBERED RELEASE
ProgramName = 'apt-untangle' #Determines only what the installed version will be called
LastNumberedRelease = '0.1.1'

#FILES AND HARDCODED SETTINGS:
ReleaseInfoFile = 'release.json'
MakefileSource = 'Makefile.in'

#IMPORTS
import argparse
import json
import os.path
import re
import sys
import subprocess

#USER INTERFACE
parser = argparse.ArgumentParser(
    description="""Configuration script for apt-untangle. WARNING: this is not an autoconf script.

This script does NOT accept the --srcdir option. If you want to build the program in a directory \
other than the source directory, do this:

cd /build/directory
/source/directory/configure

Defaults for options are printed in square brackets.

You can provide arguments for options that take them as --option ARGUMENT \\
or --option=ARGUMENT, e.g. --prefix=/usr/local and --prefix /usr/local have identical effect.\\
""".replace('\\\n', ''),
    formatter_class=argparse.RawDescriptionHelpFormatter,
    usage='%(prog)s [OPTIONS]',
    add_help=False)

group = parser.add_argument_group('Configuration')
group.add_argument('-h', '--help', action='store_true', help='print this help and exit')
group.add_argument('-V', '--version', action='store_true', help='print version information and exit')

group = parser.add_argument_group('Installation directories',
    description='In most cases simple --prefix will be enough, but options for fine-tuned control are provided.')
group.add_argument('--prefix', metavar='PREFIX', help='install all files in PREFIX [/usr/local]', default='/usr/local')
group.add_argument('--bindir', metavar='DIR', help='main script will be installed here [PREFIX/dir]')
group.add_argument('--datarootdir', metavar='DIR', help='read-only arch.-independent data root [PREFIX/share]')
group.add_argument('--mandir', metavar='DIR', help='man pages will be installed here [DATAROOTDIR/man]')

group = parser.add_argument_group('Optional features',
    description='For every feature, there is --enable-feature (or --enable-feature=YES) and --disable-feature (or --enable-feature=no)')
group.add_argument('--enable-man', metavar='yes/no', help='install man pages [yes]', default='yes', nargs='?', choices=['yes', 'no'])
group.add_argument('--disable-man', dest='enable_man', action='store_const', const='no', help=argparse.SUPPRESS)

group = parser.add_argument_group('Additional settings')
group.add_argument('--version-number', metavar='VERSION', help='force this version number for the program [default: the one in release.json or deduced]')
group.add_argument('--python-shebang', metavar='SHEBANG', default='/usr/bin/env python3', help='what shebang line to put into Python 3 scripts, without #! [/usr/bin/env python3]')

params = parser.parse_args()

def err(s):
    print(s, file=sys.stderr)

if 'PYTHON' not in os.environ:
    err('Run the configure shell script, not configure.py directly')
    sys.exit(1)

if params.help:
    parser.print_help()
    sys.exit()

srcdir = os.path.dirname(os.path.abspath(sys.argv[0]))

if srcdir:
    try:
        f = open(os.path.join(srcdir, ReleaseInfoFile))
        release = json.load(f)
        f.close()
    except FileNotFoundError:
        release = None

if params.version:
    print('{} configuration script'.format(ProgramName))
    
    if not srcdir:
        print('Sources not found')
    else:
        #Version
        if not release:
            print('This is not a numbered release')
        elif 'version' not in release:
            print('{} was found, but it contains no version info'.format(ReleaseInfoFile))
        else:
            print('Program version: {}'.format(release.version))
        
        #Latest numbered release
        if not release:
            print('Last known numbered release: {}'.format(LastNumberedRelease))
        else:
            print('Last known numbered release before this one: {}'.format(LastNumberedRelease))

    sys.exit()
    
if not srcdir:
    sys.exit(1)
    
if not params.bindir:
    params.bindir = os.path.join(params.prefix, 'bin')

if not params.datarootdir:
    params.datarootdir = os.path.join(params.prefix, 'share')

if not params.mandir:
    params.mandir = os.path.join(params.datarootdir, 'man')

version_re = re.compile('^(?:(?:(?P<epoch>[0-9]+):)?|(?=[^:]*$))(?P<upstream>[-.+:~a-zA-Z0-9]+?)(?:-(?P<debian>[+.~a-zA-Z0-9]*))?$')
if params.version_number:
    if not version_re.match(params.version_number):
        err('Provided version number does not conform to Debian standards')
        sys.exit(1)
    version = params.version_number
elif release and 'version' in release:
    if not version_re.match(release['version']):
        err('Version number in release.json does not conform to Debian standards')
        sys.exit(1)
    version = release['version']
else:
    print('Checking for hg... ', end='')
    #FIXME: subprocess.DEVNULL was added in Python 3.3. If we don't otherwise need
    #Python 3.3, we should use a previous version
    try:
        subprocess.check_call(['hg', '--version'], stdout=subprocess.DEVNULL)
        print('hg')
        version = '<not set, will be constructed automatically>'
    except (FileNotFoundError, subprocess.CalledProcessError):
        print()
        err("""We need hg (Mercurial) to automatically construct the version number.
You don't seem to have hg in $PATH.
Set the version number manually with --with-version-number=<version> or use a numbered release.""")
        sys.exit(1)

def escpath(path):
    path = os.path.normpath(path)
    path.replace("'", r"'\''")
    return "'{}'".format(path)

class AV: #Annotated value
    def __init__(self, v, ann=None):
        self.v = v
        self.ann = ann
    def __str__(self):
        return str(self.v)

Configuration = {
    'Configure script path': AV(sys.argv[0]) ,
    'Python to run configure': AV(escpath(os.environ['PYTHON'])),
    'Source directory': AV(srcdir),
    'Directory for executables': AV(escpath(params.bindir), '--bindir={}'),
    'Directory for man pages': AV(escpath(params.mandir), '--mandir={}'),
    'Enable documentation': AV((params.enable_man == 'yes'), '--enable-man={}'),
    'Compress man pages': AV(True), #For now
    'man pages are precompiled': AV(True) if (release and 'man_compiled' in release and release['man_compiled'] == True) else AV(False),
    #TODO: check the version
    'Previous numbered release': AV(LastNumberedRelease),
    'Version': AV(version, '--version-number={}' if params.version_number else None),
    'Python shebang': AV(escpath(params.python_shebang), '--python-shebang={}'),
    'PPA host': AV(''), #For now
    'Distribution codename': AV(''), #For now
    'Program name': AV(ProgramName),
    'Package name': AV(ProgramName), #For now
}

try:
    mfin = open(os.path.join(srcdir, MakefileSource))
except FileNotFoundError:
    err('{} not found in {}'.format(MakefileSource, srcdir))
    sys.exit(1)
mfout = open('Makefile', mode='w')

copying = False
print("""# DO NOT EDIT THIS FILE BY HAND
# IT HAS BEEN AUTOMATICALLY GENERATED BY {}
# ANY CHANGES WILL BE LOST IF THE PROGRAM IS RECONFIGURED
""".format(Configuration['Configure script path']), file=mfout)
for line in mfin:
    if line.startswith('#= COPY ='):
        copying = True
    elif line.startswith('#= STOP ='):
        copying = False
    elif copying:
        print(line.format_map(Configuration), end='', file=mfout)

mfin.close()
#mfout will be closed after config.status

def printconfig(prefix, file):
    for k in sorted(Configuration.keys()):
        v = Configuration[k]
        if v == True:
            v = 'yes'
        elif v == False:
            v = 'no'
        elif v == '' or v == "''":
            v = '<empty string>'
        print('{}{}: {}'.format(prefix, k, v), file=file)

with open('config.status', 'w') as f:
    print("""#!/bin/sh
# DO NOT EDIT THIS FILE BY HAND
# IT HAS BEEN AUTOMATICALLY GENERATED BY {}
# ANY CHANGES WILL BE LOST IF THE PROGRAM IS RECONFIGURED
""".format(Configuration['Configure script path']), file=f)
    printconfig('# ', f)
    print('echo RECONFIGURING', file=f)
    print('export PYTHON={}'.format(Configuration['Python to run configure']), file=f)
    param_str = ['{Python to run configure} {Configure script path}'.format_map(Configuration)]
    for k, av in Configuration.items():
        if av.ann:
            if av.v == True:
                v = 'yes'
            elif av.v == False:
                v = 'no'
            elif av.v == '':
                v = '<empty string>'
            elif k == 'Source directory':
                v = escpath(av.v)
            else:
                v = av.v
            param_str.append(av.ann.format(v))
    print(' '.join(param_str), file=f)

mfout.close()
print('\n=== CONFIGURATION ===')
printconfig('', sys.stdout)
print('CONFIGURATION FINISHED SUCCESSFULLY')    